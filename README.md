# realworld
### How to run CI/CD
1. Create a commit as usual.
2. Create a Merge Request to the `main` branch.
3. CI/CD will trigger when a new commit is in the `main` branch.

### Rollback Mechanism
This CI/CD script has a rollback mechanism, to triger we need to fill in the `ROLLBACK_IMAGE_TAG` on the .gitlab-ci file, and after that, it will automatically roll back to the image already specified.

### CI/CD Flow
The pipeline is divided into two-stage,
1. Build Stage: At this stage, we will get the commit ID, and then build the image, and tag the image based on the commit ID that we already get.
2. Deploy Stage: At this stage we will clone the terraform repo, and then change the image on the terraform with the current image tag, after that we apply the terraform, and push the terraform to the repo. 
